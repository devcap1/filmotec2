import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class rechercheFilmService {
  constructor(private http: HttpClient) { }

  public sendGetRequest(id: string){
    return this.http.get(' https://api.themoviedb.org/3/search/movie?api_key=%27clé_api%27&language=fr&query=' + id);
  }

  // working 
  getfilm(title: string) {
    return fetch(
      environment.baseURL + 'search/movie?query=' + title + '&api_key=' + environment.apiKey
    ).then(response => response.json());
  }
  
  getMovie() {
    return fetch(
      environment.baseURL + 'discover/movie?api_key=' + environment.apiKey
    ).then(response => response.json());
  }

}